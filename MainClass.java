import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.RaspiPin;

public class MainClass {
    
    public static void main(String[] args) throws InterruptedException {
        
        System.out.println("WearGenius : Getting Started with Pi4J.");
        
        final GpioController gpio = GpioFactory.getInstance();
        final GpioPinDigitalOutput led1 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_01);
		
		led1.high()
		try{
			Thread.sleep(5000);
		}catch(Exception e){
			e.printStacktrace();
		}
		led1.low();
		
    }
}